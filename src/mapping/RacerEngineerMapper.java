package mapping;

import race.Engineer;
import race.Racer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RacerEngineerMapper extends Mapper<Racer, Engineer> {
    @Override
    public Map<Racer, Engineer> map(ArrayList<Racer> racers, ArrayList<Engineer> engineers) {
        Map<Racer, Engineer> racerEngineerPair = new HashMap<>();
        for (Engineer engineer : engineers) {
            for (Racer racer : racers) {
                if (engineer.getRacer().equals(racer.getName())) {
                    racerEngineerPair.put(racer, engineer);
                }
            }
        }
        return racerEngineerPair;
    }
}
