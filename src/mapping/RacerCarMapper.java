package mapping;

import race.Car;
import race.Racer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RacerCarMapper extends Mapper<Racer, Car> {
    @Override
    public Map<Racer, Car> map(ArrayList<Racer> racers, ArrayList<Car> cars) {
        Map<Racer, Car> racerCarPair = new HashMap<>();
        for (Car car : cars) {
            for (Racer racer : racers) {
                if (car.getRacer().equals(racer.getName())) {
                    racerCarPair.put(racer, car);
                }
            }
        }
        return racerCarPair;
    }
}
