package mapping;

import java.util.ArrayList;
import java.util.Map;

public interface ObjectMapper<A, B> {
    Map<A, B> map(ArrayList<A> aArrayList, ArrayList<B> bArrayList);
}
