package parser;

import race.GrandPrix;
import util.FileUtils;

import java.util.ArrayList;

public class GrandPrixParser extends Parser<GrandPrix> {
    @Override
    public ArrayList<GrandPrix> parseFile(String fileName) {
        ArrayList<GrandPrix> grandPrixArrayList = new ArrayList<>();
        for (String line: FileUtils.readFile(fileName).split("\n")) {
            String[] fields = line.split("\\s");
            grandPrixArrayList.add(new GrandPrix(fields[0], Integer.valueOf(fields[1]), Integer.valueOf(fields[2]),
                    fields[3].split("[,]")));
        }
        return grandPrixArrayList;
    }
}
