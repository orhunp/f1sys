package parser;

import race.Team;
import util.FileUtils;

import java.util.ArrayList;

public class TeamParser extends Parser<Team> {
    @Override
    public ArrayList<Team> parseFile(String fileName) {
        ArrayList<Team> teamArrayList = new ArrayList<>();
        for (String line: FileUtils.readFile(fileName).split("\n")) {
            String[] fields = line.split("\\s");
            teamArrayList.add(new Team(fields[0], fields[1],
                    Integer.valueOf(fields[2]), Integer.valueOf(fields[3]), Integer.valueOf(fields[4])));
        }
        return teamArrayList;
    }
}
