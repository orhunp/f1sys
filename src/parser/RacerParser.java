package parser;

import race.Racer;
import util.FileUtils;

import java.util.ArrayList;

public class RacerParser extends Parser<Racer> {
    @Override
    public ArrayList<Racer> parseFile(String fileName) {
        ArrayList<Racer> racerArrayList = new ArrayList<>();
        for (String line: FileUtils.readFile(fileName).split("\n")) {
            String[] fields = line.split("\\s");
            racerArrayList.add(new Racer(Integer.valueOf(fields[0]), fields[1], fields[2], Integer.valueOf(fields[3]),
                    fields[4], Integer.valueOf(fields[5]), Integer.valueOf(fields[6]),
                    Integer.valueOf(fields[7]), Integer.valueOf(fields[8]), Integer.valueOf(fields[9])));
        }
        return racerArrayList;
    }
}
