package parser;

import cmd.Command;
import util.FileUtils;

import java.util.ArrayList;

public class CommandParser extends Parser<Command> {

    @Override
    public ArrayList<Command> parseFile(String fileName) {
        ArrayList<Command> commandArrayList = new ArrayList<>();
        for (String line: FileUtils.readFile(fileName).split("\n")) {
            if (line.length() > 3 && line.charAt(0) != '#') {
                commandArrayList.add(new Command(line));
            }
        }
        return commandArrayList;
    }
}
