package parser;

import race.Engineer;
import util.FileUtils;

import java.util.ArrayList;

public class EngineerParser extends Parser<Engineer> {
    @Override
    public ArrayList<Engineer> parseFile(String fileName) {
        ArrayList<Engineer> engineerArrayList = new ArrayList<>();
        for (String line: FileUtils.readFile(fileName).split("\n")) {
            String[] fields = line.split("\\s");
            engineerArrayList.add(new Engineer(fields[0], fields[1], fields[2], Integer.valueOf(fields[3])));
        }
        return engineerArrayList;
    }
}
