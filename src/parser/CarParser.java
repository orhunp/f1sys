package parser;

import race.Car;
import util.FileUtils;

import java.util.ArrayList;

public class CarParser extends Parser<Car> {
    @Override
    public ArrayList<Car> parseFile(String fileName) {
        ArrayList<Car> carArrayList = new ArrayList<>();
        for (String line: FileUtils.readFile(fileName).split("\n")) {
            String[] fields = line.split("\\s");
            carArrayList.add(new Car(fields[0], fields[1], Integer.valueOf(fields[2]), Integer.valueOf(fields[3]),
                    Integer.valueOf(fields[4]), fields[5], Integer.valueOf(fields[6])));
        }
        return carArrayList;
    }
}
