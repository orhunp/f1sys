package sys;

import cmd.Command;
import exception.*;
import list.RacerTimeList;
import list.TeamList;
import parser.*;
import property.TeamProperty;
import property.TeamPropertyList;
import race.*;
import util.FileUtils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class F1System implements Commands {

    private ArrayList<Command> commands;
    private TeamList teamList;
    private ArrayList<Racer> qualifiedRacers, raceWinners;
    private ArrayList<GrandPrix> grandPrixes;
    private TeamPropertyList teamPropertyList;
    private HashMap<String, RacerTimeList> races;
    private String outputPath;
    private int maxRacers;
    private int[] points;

    public F1System(String commandsPath, String teamsPath, String racersPath, String carsPath,
                    String grandPrixesPath, String engineersPath, String outputPath, int maxRacers, int[] points) {
        commands = new CommandParser().parseFile(commandsPath);
        teamList = new TeamList(new TeamParser().parseFile(teamsPath));
        qualifiedRacers = new RacerParser().parseFile(racersPath);
        raceWinners = new ArrayList<>();
        grandPrixes = new GrandPrixParser().parseFile(grandPrixesPath);
        teamPropertyList = new TeamPropertyList(new RacerParser().parseFile(racersPath),
                new CarParser().parseFile(carsPath),
                new EngineerParser().parseFile(engineersPath));
        for (Team team: teamList) {
            team.setProperties(teamPropertyList.filterForTeam(team.getName()));
        }
        races = new HashMap<>();
        this.outputPath = outputPath;
        this.maxRacers = maxRacers;
        this.points = points;
    }

    private ArrayList<Command> getCommands() {
        return commands;
    }

    public void runCommands() {
        for (Command command : getCommands()) {
            try {
                command.run(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private RacerTimeList getRacerTimeList(String tours, String timeFormat, String date, boolean eliminate) {
        ArrayList<LocalTime> times = new ArrayList<>();
        HashMap<LocalTime, Racer> racerTimes = new HashMap<>();
        for (int i = 0; i < tours.split(" - ").length; i++) {
            LocalTime time = LocalTime.parse(tours.split(" - ")[i], DateTimeFormatter.ofPattern(timeFormat));
            times.add(time);
            racerTimes.put(time, qualifiedRacers.get(i));
        }
        Collections.sort(times);
        if (eliminate) {
            FileUtils.appendToFile(outputPath, String.format("%s %s %s %s\n",
                    times.get(times.size() - 1),
                    racerTimes.get(times.get(times.size() - 1)).getName(),
                    times.get(times.size() - 2),
                    racerTimes.get(times.get(times.size() - 2)).getName()));
            qualifiedRacers.remove(racerTimes.get(times.get(times.size() - 1)));
            qualifiedRacers.remove(racerTimes.get(times.get(times.size() - 2)));
        }
        RacerTimeList racerTimeList = new RacerTimeList(times, racerTimes);
        races.put(date, racerTimeList);
        return racerTimeList;
    }

    @Override
    public void addNewGrandprix(String country, int length, int tours) {
        grandPrixes.add(new GrandPrix(country, length, tours, null));
        for (GrandPrix grandPrix: grandPrixes) {
            FileUtils.appendToFile(outputPath, String.format("%s %d %d %s\n", grandPrix.getCountry(),
                    grandPrix.getLength(), grandPrix.getTours(),
                    grandPrix.getRankings() != null ? String.join(" ", grandPrix.getRankings()) : ""));
        }
    }

    @Override
    public void modifyRaceCar(String racerName, int weight, int height, int width, int ecc) {
        try {
            teamList.getByRacer(racerName).getCarByRacer(racerName).modify(weight, height, width, ecc);
            FileUtils.appendToFile(outputPath, String.format("Modification process for %s is successful.\n",
                    teamList.getByRacer(racerName).getCarByRacer(racerName).getName()));
        } catch (CarNotFoundException | TeamNotFoundException | InvalidModificationException e) {
            e.printStackTrace();
            FileUtils.appendToFile(outputPath, "Modification failed.\n");
        }
    }

    @Override
    public void deleteTeamRacer(String teamName, int number) {
        try {
            String racerName = teamList.getByName(teamName).getRacerByNum(number).getName();
            teamList.getByName(teamName).deleteRacer(number);
            FileUtils.appendToFile(outputPath,
                    String.format("%s is deleted from team %s\n", racerName,teamName));
        } catch (TeamNotFoundException e) {
            e.printStackTrace();
        } catch (RacerNotFoundException e) {
            e.printStackTrace();
            FileUtils.appendToFile(outputPath, "Deletion failed.\n");
        }
    }

    @Override
    public void addNewF1racer(String teamName, int number, String name, String country) {
        try {
            teamList.getByName(teamName).addRacer(new Racer(number, teamName, name, 0, country, 0,
                    0, 0, 0, 0), maxRacers);
            FileUtils.appendToFile(outputPath,
                    String.format("%s is added to team %s with the number %d\n", name, teamName, number));
        } catch (TeamNotFoundException e) {
            e.printStackTrace();
        } catch (RacerLimitExceededException e) {
            e.printStackTrace();
            FileUtils.appendToFile(outputPath,
                    String.format("Addition failed. Because this team has already %d racers.\n", maxRacers));
        }
    }

    @Override
    public void exchangeRacers(int firstRacerNum, int secondRacerNum) {
        try {
            Team team1 = teamList.getByRacerNum(firstRacerNum);
            TeamProperty teamProperty1 = team1.getPropertyByRacerNum(firstRacerNum).clone();
            Team team2 = teamList.getByRacerNum(secondRacerNum);
            TeamProperty teamProperty2 = team2.getPropertyByRacerNum(secondRacerNum).clone();
            team1.deleteRacer(teamProperty1.getRacer().getNumber());
            teamProperty2.getRacer().setTeam(team1.getName());
            team1.addProperty(teamProperty2);
            team2.deleteRacer(teamProperty2.getRacer().getNumber());
            teamProperty1.getRacer().setTeam(team2.getName());
            team2.addProperty(teamProperty1);
            FileUtils.appendToFile(outputPath, String.format("%s is now in %s, %s is now in %s\n",
                    teamProperty1.getRacer().getName(), team2.getName(),
                    teamProperty2.getRacer().getName(), team1.getName()));
        } catch (TeamNotFoundException | RacerNotFoundException |
                CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void retrieveRaceEngineerInfo(String racerName) {
        try {
            Engineer engineer = teamPropertyList.getEngineerOfRacer(racerName);
            FileUtils.appendToFile(outputPath, String.format("%s %d\n",
                    engineer.getName(), engineer.getExperience()));
        } catch (EngineerNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void retrieveRaceCarInfo(String racerName) {
        try {
            Car car = teamPropertyList.getCarOfRacer(racerName);
            FileUtils.appendToFile(outputPath, String.format("%s %d %d %d %d\n",
                    car.getName(), car.getWeight(), car.getHeight(), car.getWidth(), car.getEcc()));
        } catch (CarNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void retrieveRaceResults(String date) {
        for (int i = 0; i < races.get(date).getTimes().size(); i++) {
            LocalTime time = races.get(date).getTimes().get(i);
            FileUtils.appendToFile(outputPath, String.format("%s %s %d %s ",
                    races.get(date).getRacerTimes().get(time).getName(),
                    races.get(date).getRacerTimes().get(time).getTeam(),
                    points[i],
                    time));
        }
        FileUtils.appendToFile(outputPath, "\n");
    }

    @Override
    public void qualifyingToursResultsStage1(String tours) {
       getRacerTimeList(tours,"HH:mm:ss", "10/01/2008", true);
    }

    @Override
    public void qualifyingToursResultsStage2(String tours) {
        getRacerTimeList(tours, "HH:mm:ss","24/01/2008", true);
    }

    @Override
    public void qualifyingToursResultsStage3(String tours) {
        RacerTimeList racerTimeList = getRacerTimeList(tours, "HH:mm:ss", "07/02/2008",false);
        FileUtils.appendToFile(outputPath, "Qualifying tours for this grand prix have finished.\nThe results:\n");
        for (int i = 0; i < 3; i++) {
            FileUtils.appendToFile(outputPath, String.format("Grid%d: %s\n", i + 1,
                    racerTimeList.getRacerTimes().get(racerTimeList.getTimes().get(i)).getName()));
        }
        try {
            Racer racer = racerTimeList.getRacerTimes().get(racerTimeList.getTimes().get(0));
            teamList.getByRacer(racer.getName()).incrementRacerPolePositions(racer);
        } catch (RacerNotFoundException | TeamNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void race(String tours) {
        RacerTimeList racerTimeList = getRacerTimeList(tours, "HH:mm:ss:SS", "08/02/2008", false);
        raceWinners.clear();
        for (int i = 0; i < racerTimeList.size(); i++) {
            Racer racer = racerTimeList.getRacerTimes().get(racerTimeList.getTimes().get(i));
            raceWinners.add(racer);
            try {
                teamList.getByRacer(racer.getName()).updateRacerPoints(racer, points[i], i + 1);
                teamList.getByRacer(racer.getName()).setSeasonPoints(
                        teamList.getByRacer(racer.getName()).getSeasonPoints() + points[i]);
                if (i < 3) {
                    FileUtils.appendToFile(outputPath, String.format("%d %s %d\n",
                            teamList.getByRacer(racer.getName()).getPropertyByRacerNum(
                                    racer.getNumber()).getRacer().getNumber(),
                            teamList.getByRacer(racer.getName()).getPropertyByRacerNum(
                                    racer.getNumber()).getRacer().getName(),
                            teamList.getByRacer(racer.getName()).getPropertyByRacerNum(
                                    racer.getNumber()).getRacer().getSeasonPoints()));
                }
            } catch (TeamNotFoundException | RacerNotFoundException e) {
                e.printStackTrace();
            }
        }
        grandPrixes.get(0).setRankings(raceWinners.stream().map(Racer::getName).limit(3).toArray(String[]::new));
    }

    @Override
    public void resetSeason() {
        teamList.sort(Comparator.comparingInt(Team::getSeasonPoints));
        Collections.reverse(teamList);
        for (Team team: teamList) {
            FileUtils.appendToFile(outputPath, String.format("%s %d %d %d ", team.getName(),
                    team.getSeasonPoints(), team.getTeamTitles(), team.getDriverTitles()));
        }
        FileUtils.appendToFile(outputPath, "\n");
        for (Team team: teamList) {
            team.setSeasonPoints(0);
            for (TeamProperty teamProperty: team.getProperties()) {
                teamProperty.getRacer().setExperience(teamProperty.getRacer().getExperience() + 1);
                teamProperty.getEngineer().setExperience(teamProperty.getEngineer().getExperience() + 1);
                if (raceWinners.get(0).getNumber() == teamProperty.getRacer().getNumber()) {
                    team.setDriverTitles(team.getDriverTitles() + 1);
                    team.setTeamTitles(team.getTeamTitles() + 1);
                }
                FileUtils.appendToFile(outputPath, String.format("%s %s %d %d %d %d %d ",
                        teamProperty.getRacer().getName(), teamProperty.getRacer().getTeam(),
                        teamProperty.getRacer().getSeasonPoints(), teamProperty.getRacer().getRanksInFirstThree(),
                        teamProperty.getRacer().getPolePositions(), teamProperty.getRacer().getCareerPoints(),
                        teamProperty.getRacer().getExperience()));
                teamProperty.getRacer().setSeasonPoints(0);
            }
        }
        FileUtils.appendToFile(outputPath, "\n");
    }
}
