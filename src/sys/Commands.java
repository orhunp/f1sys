package sys;

@SuppressWarnings("unused")
public interface Commands {
    void addNewGrandprix(String country, int length, int tours);
    void modifyRaceCar(String racerName, int weight, int height, int width, int ecc);
    void deleteTeamRacer(String teamName, int number);
    void addNewF1racer(String teamName, int number, String name, String country);
    void exchangeRacers(int firstRacerNum, int secondRacerNum);
    void retrieveRaceEngineerInfo(String racerName);
    void retrieveRaceCarInfo(String racerName);
    void retrieveRaceResults(String date);
    void qualifyingToursResultsStage1(String tours);
    void qualifyingToursResultsStage2(String tours);
    void qualifyingToursResultsStage3(String tours);
    void race(String tours);
    void resetSeason();
}
