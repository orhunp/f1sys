package main;

import sys.F1System;

public class Main {
    public static void main(String[] args) {
        new F1System("commands.txt",
                "team.txt",
                "team_racers.txt",
                "race_cars.txt",
                "grand_prixes.txt",
                "race_engineers.txt",
                "output.txt", 2,
                new int[]{10, 8, 6, 5, 4, 3, 2, 1}).runCommands();
    }
}

