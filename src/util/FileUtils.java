package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class FileUtils {
    public static String readFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            int c;
            while ((c = fileInputStream.read()) != -1) {
                stringBuilder.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public static void appendToFile(String fileName, String line) {
        try {
            if (!(new File(fileName).exists() || new File(fileName).createNewFile())) {
                throw new IOException("Failed to create output file");
            }
            Files.write(Paths.get(fileName), line.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
