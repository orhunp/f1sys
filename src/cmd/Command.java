package cmd;

import java.beans.Statement;
import java.util.ArrayList;

public class Command{
    private String command;

    public Command(String command) {
        this.command = command;
    }

    private String getCommand() {
        return command;
    }

    private String getMethodName() {
        StringBuilder methodName = new StringBuilder();
        String[] cmdParts = getCommand().split("[(]")[0].split("[_]");
        for (int i = 0; i < cmdParts.length; i++) {
            String part = cmdParts[i].trim();
            if (i == 0) {
                part = part.toLowerCase();
            } else {
                part = part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase();
            }
            methodName.append(part);
        }
        return methodName.toString();
    }

    private Object[] getArguments() {
        ArrayList<Object> arguments = new ArrayList<>();
        for (String arg: getCommand().split("[(]")[1]
                .replace(")", "").trim().split("[,]")) {
            try {
                int integer = Integer.valueOf(arg.trim());
                arguments.add(integer);
            } catch (NumberFormatException e) {
                if (!arg.isEmpty()) {
                    arguments.add(arg.trim());
                }
            }
        }
        return arguments.toArray();
    }

    public void run(Object target) throws Exception {
        new Statement(target, getMethodName(), getArguments()).execute();
    }
}
