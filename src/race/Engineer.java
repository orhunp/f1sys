package race;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class Engineer {
    private String name, team, racer;
    private int experience;

    public Engineer(String name, String team, String racer, int experience) {
        this.name = name;
        this.team = team;
        this.racer = racer;
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public String getRacer() {
        return racer;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
}
