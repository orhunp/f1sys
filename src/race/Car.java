package race;

import exception.InvalidModificationException;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class Car {
    private String name, racer, tyres;
    private int weight, height, width, ecc;

    public Car(String name, String racer, int weight, int height, int width, String tyres, int ecc) {
        this.name = name;
        this.racer = racer;
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.tyres = tyres;
        this.ecc = ecc;
    }

    public void modify(int weight, int height, int width, int ecc) throws InvalidModificationException {
        if (weight < 550 || height > 95 || width < 180 || ecc > 3000) {
            throw new InvalidModificationException("Invalid modification parameters");
        }
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.ecc = ecc;
    }

    public String getName() {
        return name;
    }

    public String getRacer() {
        return racer;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getEcc() {
        return ecc;
    }
}
