package race;

public class GrandPrix {
    private String country;
    private int length, tours;
    private String[] rankings;

    public GrandPrix(String country, int length, int tours, String[] rankings) {
        this.country = country;
        this.length = length;
        this.tours = tours;
        this.rankings = rankings;
    }

    public String getCountry() {
        return country;
    }

    public int getLength() {
        return length;
    }

    public int getTours() {
        return tours;
    }

    public String[] getRankings() {
        return rankings;
    }

    public void setRankings(String[] rankings) {
        this.rankings = rankings;
    }
}
