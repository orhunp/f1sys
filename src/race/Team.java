package race;

import exception.CarNotFoundException;
import exception.RacerLimitExceededException;
import exception.RacerNotFoundException;
import property.TeamProperty;
import property.TeamPropertyList;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class Team {
    private String name, country;
    private int teamTitles, driverTitles, seasonPoints;
    private TeamPropertyList properties;

    public Team(String name, String country, int teamTitles, int driverTitles, int seasonPoints) {
        this.name = name;
        this.country = country;
        this.teamTitles = teamTitles;
        this.driverTitles = driverTitles;
        this.seasonPoints = seasonPoints;
    }

    public String getName() {
        return name;
    }

    public int getSeasonPoints() {
        return seasonPoints;
    }

    public void setSeasonPoints(int seasonPoints) {
        this.seasonPoints = seasonPoints;
    }

    public int getTeamTitles() {
        return teamTitles;
    }

    public void setTeamTitles(int teamTitles) {
        this.teamTitles = teamTitles;
    }

    public int getDriverTitles() {
        return driverTitles;
    }

    public void setDriverTitles(int driverTitles) {
        this.driverTitles = driverTitles;
    }

    public TeamPropertyList getProperties() {
        return properties;
    }

    public void setProperties(TeamPropertyList properties) {
        this.properties = properties;
    }

    public void addProperty(TeamProperty property) {
        this.properties.add(property);
    }

    public Racer getRacerByNum(int number) throws RacerNotFoundException {
        for (TeamProperty property: this.getProperties()) {
            if (property.getRacer().getNumber() == number) {
                return property.getRacer();
            }
        }
        throw new RacerNotFoundException("Racer not found with the number: " + number);
    }

    public Car getCarByRacer(String racerName) throws CarNotFoundException {
        for (TeamProperty property: this.getProperties()) {
            if (property.getCar().getRacer().equals(racerName)) {
                return property.getCar();
            }
        }
        throw new CarNotFoundException("Cannot find car from racer: " + racerName);
    }

    public void deleteRacer(int number) throws RacerNotFoundException {
        for (TeamProperty property: this.getProperties()) {
            if (property.getRacer().getNumber() == number) {
                this.properties.remove(property);
                return;
            }
        }
        throw new RacerNotFoundException("Racer not found with the number: " + number);
    }

    public void addRacer(Racer racer, int racerLimit) throws RacerLimitExceededException {
        if (this.getProperties().size() < racerLimit) {
            this.properties.add(new TeamProperty(racer));
        } else {
            throw new RacerLimitExceededException(String.format("%s already has %d racers", getName(), racerLimit));
        }
    }

    public void incrementRacerPolePositions(Racer racer) throws RacerNotFoundException {
        for (int i = 0; i < this.getProperties().size(); i++) {
            Racer teamRacer = this.getProperties().get(i).getRacer();
            if (teamRacer.getName().equals(racer.getName())) {
                teamRacer.setPolePositions(
                        teamRacer.getPolePositions() + 1);
                return;
            }
        }
        throw new RacerNotFoundException("Racer not found with the name: " + racer.getName());
    }

    public void updateRacerPoints(Racer racer, int points, int position) throws RacerNotFoundException {
        for (int i = 0; i < this.getProperties().size(); i++) {
            Racer teamRacer = this.getProperties().get(i).getRacer();
            if (teamRacer.getName().equals(racer.getName())) {
                teamRacer.setSeasonPoints(
                        teamRacer.getSeasonPoints() + points);
                teamRacer.setCareerPoints(
                        teamRacer.getCareerPoints() + points);
                if (position < 4) {
                    teamRacer.setRanksInFirstThree(
                            teamRacer.getRanksInFirstThree() + 1);
                }
                if (position == 1) {
                    teamRacer.setRaceWins(
                            teamRacer.getRaceWins() + 1);
                }
                return;
            }
        }
        throw new RacerNotFoundException("Racer not found with the name: " + racer.getName());
    }

    public TeamProperty getPropertyByRacerNum(int number) throws RacerNotFoundException {
        for (TeamProperty property: this.getProperties()) {
            if (property.getRacer().getNumber() == number) {
                return property;
            }
        }
        throw new RacerNotFoundException("Racer not found with the number: " + number);
    }
}
