package race;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class Racer {
    private String name, team, country;
    private int number, experience, raceWins, careerPoints, polePositions, ranksInFirstThree, seasonPoints;

    public Racer(int number, String team, String name, int experience, String country, int raceWins,
                 int careerPoints, int polePositions, int ranksInFirstThree, int seasonPoints) {
        this.number = number;
        this.team = team;
        this.name = name;
        this.experience = experience;
        this.country = country;
        this.raceWins = raceWins;
        this.careerPoints = careerPoints;
        this.polePositions = polePositions;
        this.ranksInFirstThree = ranksInFirstThree;
        this.seasonPoints = seasonPoints;
    }

    public String getName() {
        return name;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getNumber() {
        return number;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    int getRaceWins() {
        return raceWins;
    }

    void setRaceWins(int raceWins) {
        this.raceWins = raceWins;
    }

    public int getCareerPoints() {
        return careerPoints;
    }

    void setCareerPoints(int careerPoints) {
        this.careerPoints = careerPoints;
    }

    public int getPolePositions() {
        return polePositions;
    }

    void setPolePositions(int polePositions) {
        this.polePositions = polePositions;
    }

    public int getRanksInFirstThree() {
        return ranksInFirstThree;
    }

    void setRanksInFirstThree(int ranksInFirstThree) {
        this.ranksInFirstThree = ranksInFirstThree;
    }

    public int getSeasonPoints() {
        return seasonPoints;
    }

    public void setSeasonPoints(int seasonPoints) {
        this.seasonPoints = seasonPoints;
    }
}
