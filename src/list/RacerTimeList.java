package list;

import race.Racer;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

public class RacerTimeList {
    private ArrayList<LocalTime> times;
    private HashMap<LocalTime, Racer> racerTimes;

    public RacerTimeList(ArrayList<LocalTime> times, HashMap<LocalTime, Racer> racerTimes) {
        this.times = times;
        this.racerTimes = racerTimes;
    }

    public ArrayList<LocalTime> getTimes() {
        return times;
    }

    public HashMap<LocalTime, Racer> getRacerTimes() {
        return racerTimes;
    }

    public int size() {
        return times.size();
    }
}
