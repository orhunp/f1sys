package list;

import exception.TeamNotFoundException;
import property.TeamProperty;
import race.Team;

import java.util.ArrayList;

public class TeamList extends ArrayList<Team> {

    public TeamList(ArrayList<Team> teams) {
        this.addAll(teams);
    }

    public Team getByName(String name) throws TeamNotFoundException {
        for (Team team : this) {
            if (team.getName().equals(name)) {
                return team;
            }
        }
        throw new TeamNotFoundException("Team not found with the name: " + name);
    }

    public Team getByRacer(String name) throws TeamNotFoundException {
        for (Team team : this) {
            for (TeamProperty property: team.getProperties()) {
                if (property.getRacer().getName().equals(name)) {
                    return team;
                }
            }
        }
        throw new TeamNotFoundException("Team not found with the racer name: " + name);
    }

    public Team getByRacerNum(int number) throws TeamNotFoundException {
        for (Team team : this) {
            for (TeamProperty property: team.getProperties()) {
                if (property.getRacer().getNumber() == number) {
                    return team;
                }
            }
        }
        throw new TeamNotFoundException(String.format("Team not found with the racer number: %d", number));
    }
}
