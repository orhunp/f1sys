package exception;

public class RacerNotFoundException extends Exception {
    public RacerNotFoundException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
