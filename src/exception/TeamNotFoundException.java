package exception;

public class TeamNotFoundException extends Exception {
    public TeamNotFoundException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
