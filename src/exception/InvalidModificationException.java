package exception;

public class InvalidModificationException extends Exception {
    public InvalidModificationException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
