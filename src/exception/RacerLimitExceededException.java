package exception;

public class RacerLimitExceededException extends Exception {
    public RacerLimitExceededException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
