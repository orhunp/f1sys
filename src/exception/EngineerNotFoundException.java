package exception;

public class EngineerNotFoundException extends Exception {
    public EngineerNotFoundException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
