package property;

import exception.CarNotFoundException;
import exception.EngineerNotFoundException;
import race.Car;
import race.Engineer;

public interface TeamPropertyReceiver {
    TeamPropertyList filterForTeam(String teamName);
    Engineer getEngineerOfRacer(String racerName) throws EngineerNotFoundException;
    Car getCarOfRacer(String racerName) throws CarNotFoundException;
}
