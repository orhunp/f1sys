package property;

import race.Car;
import race.Engineer;
import race.Racer;

public class TeamProperty extends Property<Racer, Car, Engineer> implements Cloneable {

    public TeamProperty(Racer racer) {
        super(racer, null, null);
    }

    TeamProperty(Racer racer, Car car, Engineer engineer) {
        super(racer, car, engineer);
    }

    public Racer getRacer() {
        return super.v1;
    }

    public Car getCar() {
        return super.v2;
    }

    public Engineer getEngineer() {
        return super.v3;
    }

    @Override
    public TeamProperty clone() throws CloneNotSupportedException {
        return (TeamProperty) super.clone();
    }
}
