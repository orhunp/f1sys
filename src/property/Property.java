package property;

abstract class Property<V1, V2, V3> {
    V1 v1;
    V2 v2;
    V3 v3;

    Property(V1 v1, V2 v2, V3 v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }
}
