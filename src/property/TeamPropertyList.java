package property;

import exception.CarNotFoundException;
import exception.EngineerNotFoundException;
import mapping.RacerCarMapper;
import mapping.RacerEngineerMapper;
import race.Car;
import race.Engineer;
import race.Racer;

import java.util.ArrayList;

public class TeamPropertyList extends ArrayList<TeamProperty> implements TeamPropertyReceiver {

    private TeamPropertyList() { }

    public TeamPropertyList(ArrayList<Racer> racers, ArrayList<Car> cars, ArrayList<Engineer> engineers) {
        for (Racer racer : racers) {
            this.add(new TeamProperty(
                    racer,
                    new RacerCarMapper().map(racers, cars).get(racer),
                    new RacerEngineerMapper().map(racers, engineers).get(racer)));
        }
    }

    @Override
    public TeamPropertyList filterForTeam(String teamName) {
        TeamPropertyList teamProperties = new TeamPropertyList();
        for (TeamProperty property : this) {
            if (property.getRacer().getTeam().equals(teamName)) {
                teamProperties.add(property);
            }
        }
        return teamProperties;
    }

    @Override
    public Engineer getEngineerOfRacer(String racerName) throws EngineerNotFoundException {
        for (TeamProperty property: this) {
            if (property.getEngineer().getRacer().equals(racerName)) {
                return property.getEngineer();
            }
        }
        throw new EngineerNotFoundException("No engineer found for " + racerName);
    }

    @Override
    public Car getCarOfRacer(String racerName) throws CarNotFoundException {
        for (TeamProperty property: this) {
            if (property.getCar().getRacer().equals(racerName)) {
                return property.getCar();
            }
        }
        throw new CarNotFoundException("No car found for " + racerName);
    }
}
